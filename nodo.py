class node():
    def __init__(self, dato):
    self.left = None
    self.right = None
    self.dato = dato   
         
    def insert(self, a, dato):
        if a == None:
            a = node(dato)
        else:
            d = a.dato
            if dato < d:
                a.left = self.insert(a.left, dato)
            else:
                a.right = self.insert(a.right, dato)
        return a

    def inorder(self, a):
        if a == None:
            return None
        else:
            self.inorder(a.left)
            print(a.dato)
            self.inorder(a.right)

    def preorder(self, a):
        if a == None:
            return None
        else:
            print(a.dato)
            self.preorder(a.left)
            self.preorder(a.right)

    def postorder(self, a):
        if a == None:
            return None
        else:
            self.postorder(a.left)
            self.postorder(a.right)
            print(a.dato)


if __name__=="__main__":
    print("Arbol binario.")
    root=None
    sentinel=True
    root = node(segments[2])
    i = 0
    valores = []
    while  i < 101 :
        particion = listar["ordens"][i]
        partede = particion.rpartition("/")
        root.insert(partede[2])
        i = i + 1

    print("Estos son los numeros enlistados.")
    print("Inorder:")
    root.inorder(valores)
    print("-".join([str(valor) for valor in valores]))
    print("Preorders")
    root.postorden(valores)
    print("-".join([str(valor) for valor in valores]))
    print("Post order:")
    root.postorden(valores)
    print("-".join([str(valor) for valor in valores]))
        

    print("de nuevo?")

